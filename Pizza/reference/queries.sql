-- CREATE
CREATE TABLE `pizza` (
                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `description` varchar(3000) COLLATE utf8mb4_unicode_ci NOT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- LIST
SELECT
    `pizza`.`id`,
    `pizza`.`name`
FROM
    `Pizza`.`pizza`;

--  DETAILS
SELECT
    `pizza`.`name`,
    `pizza`.`description`
FROM
    `Pizza`.`pizza`
WHERE
    `pizza`.`id` = :id
;

-- INSERT
INSERT INTO `Pizza`.`pizza`
(
    `id`,
    `name`,
    `description`
) VALUES (
    NULL,
    'Princesse Poopoo',
    'Princesse poopoo sur internet'
);

-- UPDATE
UPDATE `Pizza`.`pizza`
SET
    `name` = 'Princesse Poopoo',
    `description` = 'Pinrcess paapaa'
WHERE `id` = 3;

-- DELETE
DELETE FROM `Pizza`.`pizza`
WHERE `id` = 3;

