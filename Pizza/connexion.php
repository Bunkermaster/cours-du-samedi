<?php
try {
    $db = new PDO("mysql:dbname=Pizza;port=3308;host=127.0.0.1;", 'pizza', 'pizza');
    $db->exec('SET NAMES UTF8');
} catch (PDOException $exception) {
    die($exception->getMessage());
}
