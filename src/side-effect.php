<?php
/**
 * Adds $param1 and $paramb
 *
 * @param int $param1
 * @param int $paramb
 *
 * @return int
 */
function add10sion(int $param1, int $paramb): int
{
    return $param1 + $paramb;
}

function showAdd10sion(int $p1, int $pb): void
{
    echo add10sion($p1, $pb).PHP_EOL;
}

echo add10sion(12, 10).PHP_EOL;

showAdd10sion(12, 10);
