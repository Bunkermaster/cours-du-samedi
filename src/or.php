<?php

/**
 * @return bool
 */
function returnTrue(): bool
{
    echo "Trou.".DIRECTORY_SEPARATOR.PHP_EOL;

    return true;
}

/**
 * @return bool
 */
function returnFalse(): bool
{
    echo "Faulseuh.".DIRECTORY_SEPARATOR.PHP_EOL;

    return false;
}

if (returnTrue() || returnFalse()) {
    echo "fini".PHP_EOL;
} else {
    echo "Haaaaaan la honte.".PHP_EOL;
}
