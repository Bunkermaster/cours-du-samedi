<?php
/**
 * Returns an array of multibyte letters.
 *
 * @param string $string
 *
 * @return array
 */
function mb_split_string(string $string): array
{
    $length = mb_strlen($string);
    $outputarray = [];
    for ($i = 0; $i < $length; $i++) {
        $outputarray[] = mb_substr($string, $i, 1);
    }

    return $outputarray;
}

$reverseMe = "私のおしり";

$dotLength = mb_strlen($reverseMe);
$reverseMeArray = mb_split_string($reverseMe);
$middle = floor( $dotLength / 2);
for ($i = 0; $i <= $middle; $i++) {
    $temp = $reverseMeArray[$i];
    $reverseMeArray[$i] = $reverseMeArray[$dotLength - $i -1];
    $reverseMeArray[$dotLength - $i -1] = $temp;
}
unset($temp);
unset($middle);
unset($dotLength);
$reverseMe = implode('', $reverseMeArray);
unset($reverseMeArray);

echo $reverseMe;
