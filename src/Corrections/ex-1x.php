<?php
//$reverseMe = "Et qui contient... du texte";
$reverseMe = "私のおしり";

$dotLength = mb_strlen($reverseMe);
$middle = floor( $dotLength / 2);

for ($i = 0; $i <= $middle; $i++) {
    $temp = $reverseMe[$i];
    $reverseMe[$i] = $reverseMe[$dotLength - $i -1];
    $reverseMe[$dotLength - $i -1] = $temp;
}

unset($temp);
unset($middle);
unset($dotLength);

echo $reverseMe;