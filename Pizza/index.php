<?php
require_once 'connexion.php';
$detailsId = $_GET['id'] ?? null;

if (null !== $detailsId) {
    // details page
    $sql = "SELECT
    `pizza`.`name`,
    `pizza`.`description`
FROM
    `Pizza`.`pizza`
WHERE
    `pizza`.`id` = :id;";
    /** @var PDO $db */
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $detailsId, PDO::PARAM_INT);
} else  {
    // list page
    $sql = "SELECT
    `pizza`.`id`,
    `pizza`.`name`
FROM
    `Pizza`.`pizza`;";
    /** @var PDO $db */
    $stmt = $db->prepare($sql);
}
$stmt->execute();
if ($stmt->errorCode() !== '00000') {
    die("WHAT THE ACTUAL FUCK?");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pizza list</title>
</head>
<body>
<div>
<?php
if (null !== $detailsId) {
    require 'includes/details.php';
} else {
    require 'includes/list.php';
}
?>
</div>
</body>
</html>
