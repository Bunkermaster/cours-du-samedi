<?php
/*
 * Double fromage
 * Olive
 * Anchois
 * Sauce tomate
 * Chèvre
 */

// labels
define("LABEL_DOUBLE_CHEESE", 'Double fromage');
define("LABEL_OLIVE",         'Olive');
define("LABEL_ANCHOVIES",     'Anchois');
define("LABEL_TOMATO_MASH",   'Sauce tomate ');
define("LABEL_GOAT_CHEESE",   'Chèvre');

// flags
//define("TOPPING_DOUBLE_CHEESE", 0b00000001);
//define("TOPPING_OLIVE",         0b00000010);
//define("TOPPING_ANCHOVIES",     0b00000100);
//define("TOPPING_TOMATO_MASH",   0b00001000);
//define("TOPPING_GOAT_CHEESE",   0b00010000);
define("TOPPING_DOUBLE_CHEESE", 1);
define("TOPPING_OLIVE",         2);
define("TOPPING_ANCHOVIES",     4);
define("TOPPING_TOMATO_MASH",   8);
define("TOPPING_GOAT_CHEESE",   16);

$pizza = TOPPING_OLIVE | TOPPING_GOAT_CHEESE | TOPPING_TOMATO_MASH;
//$pizza = 0;

echo decbin($pizza).PHP_EOL;

echo describePizza($pizza);

function describePizza(int $pizza): string
{
    $returnBuffer = '';
    if (0 === $pizza){
        $returnBuffer .= "Y'a quedalle sur ta pizza frere";
    } else {
        if (TOPPING_DOUBLE_CHEESE === ($pizza & TOPPING_DOUBLE_CHEESE)) {
            $returnBuffer .= LABEL_DOUBLE_CHEESE.PHP_EOL;
        }
        if (TOPPING_OLIVE === ($pizza & TOPPING_OLIVE)) {
            $returnBuffer .= LABEL_OLIVE.PHP_EOL;
        }
        if (TOPPING_ANCHOVIES === ($pizza & TOPPING_ANCHOVIES)) {
            $returnBuffer .= LABEL_ANCHOVIES.PHP_EOL;
        }
        if (TOPPING_TOMATO_MASH === ($pizza & TOPPING_TOMATO_MASH)) {
            $returnBuffer .= LABEL_TOMATO_MASH.PHP_EOL;
        }
        if (TOPPING_GOAT_CHEESE === ($pizza & TOPPING_GOAT_CHEESE)) {
            $returnBuffer .= LABEL_GOAT_CHEESE.PHP_EOL;
        }
    }

    return $returnBuffer;
}
