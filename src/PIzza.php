<?php
/*
 * Double fromage
 * Olive
 * Anchois
 * Sauce tomate
 * Chèvre
 */

use phpDocumentor\Reflection\Types\Boolean;

class Pizza
{
    /** @var Boolean */
	private $doubleFromage;
    /** @var Boolean */
	private $olive;
    /** @var Boolean */
	private $anchois;
    /** @var Boolean */
	private $sauceTomate;
    /** @var Boolean */
	private $chevre;

    /**
     * Pizza constructor.
     *
     * @param Boolean $doubleFromage
     * @param Boolean $olive
     * @param Boolean $anchois
     * @param Boolean $sauceTomate
     * @param Boolean $chevre
     */
    public function __construct(
        Boolean $doubleFromage,
        Boolean $olive,
        Boolean $anchois,
        Boolean $sauceTomate,
        Boolean $chevre
    ) {
        $this->doubleFromage = $doubleFromage;
        $this->olive = $olive;
        $this->anchois = $anchois;
        $this->sauceTomate = $sauceTomate;
        $this->chevre = $chevre;
    }

    /**
     * @return Boolean
     */
    public function getDoubleFromage(): Boolean
    {
        return $this->doubleFromage;
    }

    /**
     * @param Boolean $doubleFromage
     * @return Pizza
     */
    public function setDoubleFromage(Boolean $doubleFromage): Pizza
    {
        $this->doubleFromage = $doubleFromage;
        return $this;
    }

    /**
     * @return Boolean
     */
    public function getOlive(): Boolean
    {
        return $this->olive;
    }

    /**
     * @param Boolean $olive
     * @return Pizza
     */
    public function setOlive(Boolean $olive): Pizza
    {
        $this->olive = $olive;
        return $this;
    }

    /**
     * @return Boolean
     */
    public function getAnchois(): Boolean
    {
        return $this->anchois;
    }

    /**
     * @param Boolean $anchois
     * @return Pizza
     */
    public function setAnchois(Boolean $anchois): Pizza
    {
        $this->anchois = $anchois;
        return $this;
    }

    /**
     * @return Boolean
     */
    public function getSauceTomate(): Boolean
    {
        return $this->sauceTomate;
    }

    /**
     * @param Boolean $sauceTomate
     * @return Pizza
     */
    public function setSauceTomate(Boolean $sauceTomate): Pizza
    {
        $this->sauceTomate = $sauceTomate;

        return $this;
    }

    /**
     * @return Boolean
     */
    public function getChevre(): Boolean
    {
        return $this->chevre;
    }

    /**
     * @param Boolean $chevre
     * @return Pizza
     */
    public function setChevre(Boolean $chevre): Pizza
    {
        $this->chevre = $chevre;

        return $this;
    }


}

$pizza = new pizza(true, true, true, true, true);
$pizza->setChevre(false)
    ->setOlive(false);
